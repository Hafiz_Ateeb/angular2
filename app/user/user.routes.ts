/**
 * Created by Hafiz Ateeb on 3/13/2017.
 */
import {ProfileComponent} from './profile.component'
import {LoginComponent} from "./login.component";

export const userRoutes = [
    { path: 'profile', component: ProfileComponent },
    { path: 'login', component: LoginComponent }
]