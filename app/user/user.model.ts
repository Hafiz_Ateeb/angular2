/**
 * Created by Hafiz Ateeb on 3/14/2017.
 */
export interface IUser {
    id: number,
    firstName: string,
    lastName: string,
    userName: string
}