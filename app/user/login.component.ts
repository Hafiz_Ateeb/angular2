/**
 * Created by Hafiz Ateeb on 3/14/2017.
 */
import {Component} from '@angular/core'
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";

@Component({
    templateUrl: 'app/user/login.component.html',
    styles: [`
        em {float:right; padding-left: 10px; color: E05C65;}
    `]
})
export class LoginComponent{

    constructor(private authService: AuthService, private router:Router) {

    }

    login(formValues){
        this.authService.loginUser(formValues.userName, formValues.password)
        this.router.navigate(['events'])
    }

    cancel(){
        this.router.navigate(['events'])
    }
}